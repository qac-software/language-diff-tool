FROM maven:alpine

# create app dir
WORKDIR /usr/src/app

ENV FC_LANG en-US
ENV LC_CTYPE en_US.UTF-8

RUN apk update && apk add ttf-dejavu

COPY pom.xml ./

RUN mvn dependency:resolve-plugins dependency:resolve clean install

COPY . ./

RUN mvn clean install -Dmaven.test.skip=true -e

ENTRYPOINT [ "java", "-jar", "target/languagedifftool-0.0.1_alpha.jar"]
