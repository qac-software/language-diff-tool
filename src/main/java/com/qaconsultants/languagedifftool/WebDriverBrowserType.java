package com.qaconsultants.languagedifftool;

public enum WebDriverBrowserType 
{
	CHROME,
	FIREFOX,
	IE,
	EDGE,
	PHANTOMJS,
	SAFARI,
	OPERA;
}
