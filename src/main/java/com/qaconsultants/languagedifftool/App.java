package com.qaconsultants.languagedifftool;

import java.util.List;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;


/***
 * This program goes to 2 urls and highlights 
 * all text on the second webpage that appears 
 * in the first and is not on the ignore list
 * @author Alex
 *
 */
public class App 
{
	private static WebDriver driver = null;
	public static String versionString = "Language Diff Tool v1.1.13";

    public static void main( String[] args )
    {
    	
//    	Util.getRunTimeStamp();
//    	System.out.println(Util.getRunTimeStampForReport());
    	
    	String configFileName = "";
    	if (args.length > 0)
    	{
    	assert args[0] != null : "config filepath cannot be null";
    	assert args[0] != "" : "config filepath cannot be blank";
    	}
    	else
    	{
    		System.out.println("please provide the filepath for the config file as an argument (java -jar languagedifftool-0.0.1_alpha.jar <filepath to config.toml>)");
    		System.exit(1);
    	}

    	// parse config file
    	configFileName = args[0];
    	
    	if (args[0].equals("config.toml"))
    	{
    		configFileName = Util.getFilePath("config", "config.toml");
    	}
    	
    	System.out.println(versionString);
    	System.out.println();
    	Util.parseTomlConfig(configFileName);
    	
    	Util.validateConfigValues();
    	
    	Util.printURLInfoToFile();

    	// get ignore list
    	List<String> ignoreList = Util.getIgnoreListFromFile();
    	
    	Util.printElementsHeaderToFile();
    	
    	//WebDriver Setup
    	driver = Util.browserFactory();
		
    	//gather english text
		Util.getElementsToClickFromURL(driver, Config.getInstance().englishURL);
		String[] englishDictionary = Util.generateEnglishDictionary(driver);
		System.out.println(Arrays.toString(englishDictionary));
		
    	//gather non-english elements
		Util.getElementsToClickFromURL(driver, Config.getInstance().otherURL);
    	
		//process elements
		ListGatheredWebElement gatheredNonEnglishElements = Util.gatherNonEnglishWebElementsFromPage(driver);
    	gatheredNonEnglishElements.ProcessElements(englishDictionary, ignoreList);
    	
    	gatheredNonEnglishElements.flagDuplicates();
    	
    	//highlight and screenshot
    	Util.highlightElementsAndScreenShot(driver, gatheredNonEnglishElements);
    	
		//close WebDriver
		driver.quit();
		
		Util.createZipFromOutputFolder();
    }
}
