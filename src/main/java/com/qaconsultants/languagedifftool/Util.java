package com.qaconsultants.languagedifftool;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.moandjiezana.toml.Toml;

/***
 * Util holds all the static Utility functions for the LanguageDiffTool
 * @author Alex
 *
 */
public class Util 
{
	private static long screenshotID = 0;
	private static String runTimeStamp = "";
	private static int dotIndex = 0;
	private static boolean writtenURLonce = false;
	
	public static String newLine = System.getProperty("line.separator");
	

	
	/***
	 * 
	 * parseTomlConfig parses the TOML file at 
	 * _configFileName and places the contents 
	 * into the Config class
	 * 
	 * @param _configFileName
	 */
	
	public static void parseTomlConfig(String _configFilePath)
	{
		File tomlFile = new File(_configFilePath);
		
		//ignore unused warning. This command loads values into the Config Class
		@SuppressWarnings("unused")
		Config config = new Toml().read(tomlFile).to(Config.class);
	}
		
	/***
	 * This function checks if Maven or another source 
	 * (command line, eclipse) is running the project
	 * 
	 * @return true if Maven is running the project. False if eclipse or command line is
	 */
	public static boolean runningMaven()
	{
		return System.getProperty("java.class.path").contains("languagedifftool-");
	}
	
	/***
	 * This function builds a relative filepath 
	 * regardless of execution point (Maven/other)
	 * 
	 * @param _folder the folder in the project folder
	 * @param _filename the file name. leave blank string for no file
	 * @return the complete filepath
	 */
	public static String getFilePath(String _folder, String _filename)
	{
		return Util.runningMaven() ? ".\\..\\" + _folder + "\\" + _filename : ".\\" + _folder + "\\" + _filename;
	}
	
	/***
	 * This function builds the required driver based on the WebDriverBrowserType enum
	 * @param _browser the WebDriverBrowserType to build
	 * @return the WebDrivber of type _browser
	 */
	public static WebDriver browserFactory() 
    {
    	WebDriver webDriver = null;
    	
    	if (Config.getInstance().remote.useRemote)
    	{
    		System.out.println(Config.getInstance().remote.remoteAddress);
    		try 
    		{
    		webDriver = new RemoteWebDriver(new URL(Config.getInstance().remote.remoteAddress), DesiredCapabilities.chrome());
    		}
    		catch(MalformedURLException mue)
    		{
    			System.out.println("failed trying to connect to grid server at: " + Config.getInstance().remote.remoteAddress);
    		}
    		catch (Exception ex)
    		{
    			System.out.println("Something went wrong while setting up the remote web driver");
    		}
    	}
    	else
    	{
    		String filePath;
        	//DEBUG//System.out.println(Config.getInstance().hiddenConfig.chromeDriverFilename);
    		filePath = getFilePath("webdrivers", Config.getInstance().hiddenConfig.chromeDriverFilename);
    		System.setProperty("webdriver.chrome.driver", filePath);
    		webDriver = new ChromeDriver();
    	}
    	
    	
    	
    	prepareDriver(webDriver);
    	
		return webDriver;
	}
	
	/***
	 * This function returns the timestamp captured initially at run-time
	 * @return
	 */
	public static String getRunTimeStamp()
	{
		if (runTimeStamp.equals("") || runTimeStamp.equals(null))
		{
			try 
	    	{
				runTimeStamp = getTimeStamp();
			} 
	    	catch (IOException ioException) 
	    	{
				ioException.printStackTrace();
			}
		}
		
		return runTimeStamp;
    	
	}
	
	/***
	 * This functions scrolls to _element and screenshots it
	 * @param _driver the WebDriver
	 * @param _element the WebElement
	 */
	public static void scrollAndScreenshotElement(WebDriver _driver, WebElement _element, int _id)
    {
		if(_element.isDisplayed() && _element.isEnabled())
		{
			scrollToTopOfPageWithJS(_driver);
			
	    	//scrollElementIntoView(_driver, _element);
			int yScrollValue =  _element.getLocation().getY();
			scrollToCoordinateY(_driver, yScrollValue);
	    	
	    	//String filePath = getFilePath("screenshots", "");
	    	String filePath = Config.getInstance().pathing.outputDir.replace("/", File.separator);
	    			//Util.runningMaven() ? ".\\..\\screenshots\\" : ".\\screenshots\\";  	
	    	
	    	byte[] img_bytes = ((TakesScreenshot)_driver).getScreenshotAs(OutputType.BYTES);
	    	
	    	try 
	    	{
				BufferedImage screenshotImage = ImageIO.read(new ByteArrayInputStream(img_bytes));
				
				// add some text and draw a rectangle
				Graphics graphicsObject = screenshotImage.getGraphics();
				graphicsObject.setColor(Color.red);
				graphicsObject.setFont(new Font( "SansSerif", Font.BOLD, 16));
				
				graphicsObject.drawRect(0, 0, 50, 20);
				
				graphicsObject.drawString("id: " + _id, 0, 16);
				
				graphicsObject.dispose();

				long webElementID = getNextUniqueID();
				String webElementIDString = (webElementID <= 9)? "0" + webElementID : "" + webElementID ;
				String screnshotName = filePath + webElementIDString + " - id-" + _id +  " - " + getTimeStamp() + ".png";
				// save the image
				File screenshotFile = new File(screnshotName);
				System.out.println("screenshot path: " + screnshotName);
				screenshotFile.mkdirs();
				ImageIO.write(screenshotImage, "png", screenshotFile);

			} 
	    	catch (IOException ioException) 
	    	{
				ioException.printStackTrace();
			}
	    	
		}
		else
		{
			System.out.println("1 Element found was invisible... skipping screenshot. had text: \"" + _element.getText() + "\"");
		}
    }
    
	/***
	 * This function returns unique IDs for individual runs
	 * note: they reset each time. used for screenshots
	 * @return the unique ID
	 */
    private static long getNextUniqueID()
    {
    	return ++screenshotID;
    }
    
    /***
     * This function returns a formatted timestamp. 
     * ready for files/paths
     * yyyy-MM-dd __hh_mm_ss_SS__
     * 
     * @return a formatted timeStamp
     * @throws IOException
     */
    private static String getTimeStamp()throws IOException
    {
        Calendar calendar = Calendar.getInstance();       
        Date time=calendar.getTime();
        SimpleDateFormat formattedDate = new SimpleDateFormat("yyyy-MM-dd __hh_mm_ss_SS__");
        String timestamp = formattedDate.format(time);
        
        return timestamp;
    }
    /***
     * This function returns a formatted timestamp. 
     * ready for the report
     * Date January 24, 2017
     * Time 10:19 am
     * 
     * @return a formatted timeStamp
     * @throws IOException
     */
    private static String getTimeStampReportFormat()
    {
        Calendar calendar = Calendar.getInstance();       
        Date datetime=calendar.getTime();
        SimpleDateFormat formattedDate = new SimpleDateFormat("MMM d, yyyy");
        SimpleDateFormat formattedtime = new SimpleDateFormat("hh:mm a");
        String timestamp = "Date " + formattedDate.format(datetime) + Util.newLine + "Time " + formattedtime.format(datetime);
        
        return timestamp;
    }
    
    
    /***
     * This function highlights an element by changing its colour
     * and puts a border around it
     * @param _driver the WebDriver
     * @param _ele the Element to highlight
     * @param _color the highlight colour
     * @param _borderThickness the highlight border thickness
     * @throws Exception
     */
    public static void highlightElement(WebDriver _driver, WebElement _ele, String _color, int _borderThickness) throws Exception
    {
        try
        {
        	//use JavaScript to mess with the DOM
            JavascriptExecutor js = (JavascriptExecutor) _driver;
            js.executeScript("arguments[0].style.color = \"" + _color + "\"", _ele);
            js.executeScript("arguments[0].style.border = \"" + _borderThickness + "px solid " + _color + "\"", _ele);
        }
        catch (Throwable t)
        {
            throw new Exception("Failed to highlight web-element: " + _ele.getTagName() + "|" + _ele.getText(), null);
        }
    }
    
    /***
     * This function unhighlights an element by changing its colour back
     * and removes the border around it
     * @param _driver the WebDriver
     * @param _ele the Element to highlight
     * @param _color the highlight colour
     */
    public static void unhighlightElement(WebDriver _driver, WebElement _ele, String _color)
    {
        try
        {
        	//use JavaScript to mess with the DOM
            JavascriptExecutor js = (JavascriptExecutor) _driver;
            js.executeScript("arguments[0].style.color = \"" + _color + "\"", _ele);
            js.executeScript("arguments[0].style.border = \"" + 0 + "px solid " + _color + "\"", _ele);
        }
        catch (Exception ex)
        {
          System.out.println(ex.getMessage());
        }
    }
    
    private static String removePunctuation (String _text)
    {
    	String output = _text;
    	List<String> punctuation = Config.getInstance().punctuationToIgnore;
    		
			for (String punct : punctuation)
			{
				//DEBUG//System.out.println("punct: " + punct);
				output = output.replaceAll(punct, " ");
			}
    	
    	return output;
    }
    
    public static void printURLInfoToFile()
    {
        //String filePath = getFilePath("screenshots", "");
		//String outputPath = filePath + getRunTimeStamp() + "\\" + "Report.txt";
    	
    	String filePath = Config.getInstance().pathing.outputDir.replace("/", File.separator);
    	String outputPath = filePath + File.separator + "Report.txt";

    	//System.out.println("------------------------> " + outputPath);
    	
		Path _path = Paths.get(outputPath);
		//System.out.println("does path exist? " + Files.exists(_path));
		
		if(!Files.exists(_path))
		{
			try
			{
				//System.out.println("parentDir = " + _path.getParent());
				//System.out.println("path after createDirs: " + Files.createDirectories(_path.getParent()));
				Files.createFile(_path);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		try
		{
			FileOutputStream _fs = new FileOutputStream(outputPath, true);
			Util.writeURLInformation(_fs);
			_fs.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }
    
    public static String StringToTitleCase(String _text)
    {
    	return ("" + _text.charAt(0)).toUpperCase() + _text.substring(1);
    }

    private static void writeURLInformation(FileOutputStream _fs)
	{
		StringBuilder _builder = new StringBuilder();
		_builder.append(App.versionString);
		_builder.append(" Report");
		_builder.append(newLine);
		_builder.append("----------------------------");
		_builder.append(newLine);
		_builder.append(newLine);
		
		_builder.append(Util.getTimeStampReportFormat());
		_builder.append(newLine);
		_builder.append(newLine);
		_builder.append(newLine);
		
		_builder.append("English URL: \t");
		_builder.append(Config.getInstance().englishURL);
		_builder.append(newLine);
		_builder.append(newLine);

        _builder.append(Util.StringToTitleCase(Config.getInstance().language) + " URL: \t");
		_builder.append(Config.getInstance().otherURL);
		_builder.append(newLine);
		_builder.append(newLine);
		_builder.append(newLine);

		try 
		{
			if (!writtenURLonce)
			{
			_fs.write(_builder.toString().getBytes(StandardCharsets.UTF_8));
			writtenURLonce = true;
			_fs.close();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
    
    public static void writeIgnoreListToFile(List<String> commonKeysRevised, FileOutputStream _fs)
	{
    	boolean firstRow = true;
    	
    	StringBuilder _builder = new StringBuilder();
    	_builder.append(newLine);
    	for (String key : commonKeysRevised)
		{
    		if (firstRow)
    		{
    		_builder.append(key);
    		_builder.append(newLine);
    		//_builder.append("[");
    		firstRow = false;
    		}
    		else
    		{
        		//_builder.append("\"");
    			_builder.append(key);
        		//_builder.append("\"");
        	//	_builder.append(", ");
        		_builder.append(newLine);
    		}
    	}
		//_builder.append("]");
    	_builder.append(newLine);
    	_builder.append(newLine);
		try
		{
			_fs.write(_builder.toString().getBytes(StandardCharsets.UTF_8));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
    
    
    public static void writeStringListToOutputFile(List<String> commonKeysRevised, String _reportHeader)
	{
    	List<String> outputList = new ArrayList<String>(commonKeysRevised);
    	String filePath = Config.getInstance().pathing.outputDir.replace("/", File.separator);
    	String outputPath = filePath + File.separator + "Report.txt";
		Path _path = Paths.get(outputPath);
		
		if(!Files.exists(_path))
		{
			try
			{
				Files.createDirectories(_path.getParent());
				Files.createFile(_path);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		outputList.add(0, _reportHeader);
		try
		{
			FileOutputStream _fs = new FileOutputStream(outputPath, true);
			Util.writeIgnoreListToFile(outputList, _fs);
			_fs.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
    
    public static void writeStringToOutputFile(String _text, FileOutputStream _fs)
	{
    	//System.out.println("text to write: " + _text);
    	StringBuilder _builder = new StringBuilder();

		_builder.append(_text);
		_builder.append(newLine);
		_builder.append(newLine);

		
		try
		{
			_fs.write(_builder.toString().getBytes(StandardCharsets.UTF_8));
			_fs.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}

	/***
     * This function takes in a URL and returns a 
     * list of all text based elements from the 
     * tags specified in config.toml
     * 
     * @param _driver the WebDriver
     * @param _url the url
     * @return a list of all Elements with text from the url
     */
    public static List<WebElement> getElementsFromURL(WebDriver _driver, String _url)
    {
    	try
		{
    		int SecondsToWait = Config.getInstance().secondsToWait;
    		int millisToWait = SecondsToWait * 1000;
			Thread.sleep(millisToWait);
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//get list of tags to process through
		List<String> tags = Config.getInstance().tagsToProcess;

		List<WebElement> elements = new ArrayList<WebElement>();

		for(String tag: tags)
		{
			if (!Config.getInstance().onlyLookInsideElement.equals(""))
			{
				elements.addAll(_driver.findElement(By.className(Config.getInstance().onlyLookInsideElement)).findElements(By.tagName(tag)));
			}
			else
			{
				elements.addAll(_driver.findElements(By.tagName(tag)));
			}
		}
		

		return elements;
    }
    
    /***
     * This function takes in a URL and returns a 
     * list of all text based elements from the 
     * tags specified in config.toml
     * 
     * @param _driver the WebDriver
     * @param _url the url
     * @return a list of all Elements with text from the url
     */
    public static void getElementsToClickFromURL(WebDriver _driver, String _url)
    {
		//point the WebDriver to the url
    	_driver.get(_url);
		
    	//interact with elements to display all elements
    	List<String> classNamesToClick = Config.getInstance().clickClassNames;
    	//System.out.println(classNamesToClick);
    	
    	List<WebElement> elementsToInteract = new ArrayList<WebElement>();
    	
    	for (String className: classNamesToClick)
    	{
    		elementsToInteract.addAll(_driver.findElements(By.className(className)));

    	}
    	
    	for(WebElement element:elementsToInteract)
    	{
    		if (element.isEnabled())
    		{
	    		scrollToTopOfPageWithJS(_driver);
	    		
	    		if (element.isDisplayed())
	    		{
	    			element.click();
	    			Util.unselectElement(_driver, element);
	    		}
	    		
	    		try 
	    		{
					Thread.sleep(1000);
				} 
	    		catch (InterruptedException e) 
	    		{
					e.printStackTrace();
				}
    		}
    		
    	}
    	_driver.findElement(By.tagName("body")).click();
    	
    }
    
    public static void printDot()
    {
    	printDot('.');
    }
    
    public static void printDot(Character _dot)
    {
    	System.out.print(_dot);
    	if (++dotIndex >= Config.getInstance().hiddenConfig.dotsPerLine)
    	{
    		resetDotIndex();
    		System.out.println();
    	}
    }
    
    public static void resetDotIndex()
    {
    	dotIndex = 0;
    }
    
    /***
     * This function returns the ignore 
     * list based on the configured language
     * 
     * @return the ignore list
     */
    public static List<String> getIgnoreListFromFile()
    {
    	System.out.println("\nRetrieving ignore list(s)..");
    	
    	List<String> ignoreList = new ArrayList<String>();
    	
    	List<String> pageSpecificIgnore;
		
		try
		{
    		//add page specific ignore values from Config
			pageSpecificIgnore = FileUtils.readLines(new File(Config.getInstance().pathing.ignoreFileDir + "ignore.txt"), "UTF-8");
			
			//append ignore file
			pageSpecificIgnore.addAll(Config.getInstance().pageSpecificIgnoreList);
			
			for (String ignoreWord : pageSpecificIgnore)
	    	{
	    		ignoreList.add(ignoreWord);
	    	}
	    	
	    	Util.writeStringListToOutputFile(ignoreList, "Page Specific Ignore List:");
		}
		catch(NullPointerException npe)
		{
			System.out.println("Could not locate page specific ignore list in config.toml");
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	try
		{

	    	String language = Config.getInstance().language;
	    	
	    	
	    	
	    	//add ignore list from Config
	    	List<String> languageSpecificIgnoreList = new ArrayList<String>();
	    	
	    	switch(language)
	    	{
	    	case "french":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().frenchIgnoreList);
	    	break;
	    	case "german":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().germanIgnoreList);
	    	break;
	    	case "arabic":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().arabicIgnoreList);
	    	break;
	    	case "japanese":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().japaneseIgnoreList);
	    	break;
	    	case "chinese":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().chineseIgnoreList);
	    	break;
	    	case "spanish":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().spanishIgnoreList);
	    	break;
	    	case "russian":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().russianIgnoreList);
	    	break;
	    	case "italian":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().italianIgnoreList);
	    	break;
	    	case "korean":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().koreanIgnoreList);
	    	break;
	    	case "portuguese":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().portugueseIgnoreList);
	    	break;
	    	case "brazilianportuguese":
	    		languageSpecificIgnoreList.addAll(Config.getInstance().brazilianPortugueseIgnoreList);
	    	break;
	    	}
	    	
	    	//append ignore list from file
	    	languageSpecificIgnoreList.addAll(FileUtils.readLines(new File(Config.getInstance().pathing.ignoreFileDir + language +  "_ignore.txt"), "UTF-8"));

	    
	    	assert (languageSpecificIgnoreList != null) : "languageSpecificIgnoreFile was never populated. please ensure you have selected a valid language";
	    	ignoreList.addAll(languageSpecificIgnoreList);
			Util.writeStringListToOutputFile(languageSpecificIgnoreList, Util.StringToTitleCase(language) + " Ignore List:");
		
			boolean languageSupported = false;
		    for (String languageOption : Config.getInstance().hiddenConfig.supportedLanguages)
		    {
		    	if (language.equals(languageOption))
		    	{
		    		languageSupported = true;
		    		break;
		    	}
		    }
		    
		    if (!languageSupported)
		    {
		    	 System.out.println(String.format("\nLanguage not supported - No ignore-list could be found for the provided language: %s. " +
		 				"\nSupported languages: French, German, Arabic, Chinese, Japanese, Spanish, Russian, Italian, Korean, Portuguese, BrazilianPortuguese\n", language));
		 		System.exit(1);
		    }
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	System.out.println(ignoreList);
		System.out.println("\nFinished gathering ignore list(s).");
    	return ignoreList;
    }

    /***
     * This function scrolls to the bottom 
     * of the page using JavaScript
     * @param _driver the WebDriver
     */
    public static void scrollToBottomOfPageWithJS(WebDriver _driver)
    {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)_driver;
        javascriptExecutor.executeScript("window.scrollTo(0, document.body.scrollHeight);");
    }

    /***
     * This function scrolls to the top 
     * of the page using JavaScript
     * @param _driver the WebDriver
     */
    public static void scrollToTopOfPageWithJS(WebDriver _driver)
    {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)_driver;
        javascriptExecutor.executeScript("window.scrollTo(0, 0);");
    }

    /**
     * This function scrolls to a WebElement 
     * on the page using actions
     * @param _driver the WebDriver
     * @param locator the By locator to locate the WebElement
     */
    public static void scrollDownToElement(WebDriver _driver, By locator)
    {
        WebElement element = _driver.findElement(locator);
        scrollDownToElement(_driver, element);
    }
    
    /***
     * This function scrolls to a WebElement 
     * on the page using actions
     * @param _driver the WebDriver
     * @param _element the WebElement
     */
    public static void scrollDownToElement(WebDriver _driver, WebElement _element)
    {
        Actions actions = new Actions(_driver);
        actions.moveToElement(_element);
        actions.perform();
    }

    /***
     * This function scrolls down to a 
     * WebElement using Javascript and 
     * does it incrementally
     * 
     * @param _driver the WebDriver
     * @param locator the By locator used to find the Element
     * @param increment how far to scroll per tick
     */
    public static void scrollDownToElementWithJS(WebDriver _driver, By locator, int increment)
    {
        WebElement element = _driver.findElement(locator);
        scrollDownToElementWithJS(_driver,element,increment);
    }
    
    /***
     * This function scrolls down to a 
     * WebElement using Javascript and 
     * does it incrementally
     * 
     * @param _driver the WebDriver
     * @param _element the WebElement
     * @param increment how far to scroll per tick
     */
    public static void scrollDownToElementWithJS(WebDriver _driver, WebElement _element, int increment)
    {
        int currentY = 0;
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)_driver;
       
        boolean found = false; 

        while (!found)
        {
            try
            {
                found = _element.isDisplayed();
            }
            catch (Exception ex)
            {
                javascriptExecutor.executeScript("window.scrollTo(" + 0 + ", " + currentY + ");");
                currentY += increment;
            }
        }
    }
    
    public static void unselectElement(WebDriver _driver, WebElement _element)
    {
        // document.getElementById("orange").selected = true;
        ((JavascriptExecutor)_driver).executeScript("arguments[0].selected = false;", _element);
    }
    
    public static void scrollToCoordinateY(WebDriver _driver, int yValue)
    {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)_driver;
        
        yValue = (yValue >= 100) ? yValue-= 150 : 0;

        javascriptExecutor.executeScript("window.scrollTo(" + 0 + ", " + (yValue) + ");");
    }

    /***
     * This function scrolls an element into view
     * @param _driver the WebDriver
     * @param element the WebElement
     */
    public static void scrollElementIntoView(WebDriver _driver, WebElement element)
    {
        ((JavascriptExecutor)_driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }
    
    /**
     * this private helper function asserts that both URLs (input from Config)
     * 
     */
	private static void confirmURLs() 
	{
    	assert !Config.getInstance().englishURL.equals(null): "English URL was null. English URL cannot be null";
    	assert !Config.getInstance().otherURL.equals(null): "Non-English URL was null. Non-English URL cannot be null";
    	assert !Config.getInstance().englishURL.equals(""): "English URL was blank. English URL cannot be blank";
    	assert !Config.getInstance().otherURL.equals(""): "Non-English URL was blank. Non-English URL cannot be blank";
	}

	/**
	 * this private helper function asserts that the driver is NOT null (a null driver is a bad idea)
	 * 
	 * @param _driver
	 */
	private static void confirmDriver(WebDriver _driver) 
	{
    	assert (!_driver.equals(null)): "selenium webdriver was null. driver cannot be null";
	}
	
	/**
	 * this private helper function  asserts that the driver and URLs 
	 * found in config are not null and not null/blank respectively
	 * 
	 * @param _driver
	 */
	private static void confirmDriverandURLs(WebDriver _driver)
	{
		confirmDriver(_driver);
		confirmURLs();
	}

	/**
	 * this function asserts that the driver and URLs are valid input (NOT null and NOT blank "")
	 * @param _driver
	 */
	private static void prepareDriver(WebDriver _driver) 
	{
		confirmDriverandURLs(_driver);
		_driver.manage().window().maximize();
	}

	public static String[] generateEnglishDictionary(WebDriver _driver)
	{
		List<String> tags = Config.getInstance().tagsToProcess;

		List<WebElement> elements = new ArrayList<WebElement>();
		List<String> nonUniqueText = new ArrayList<String>();
		List<String> nonUniqueWords = new ArrayList<String>();

		try
		{
    		int SecondsToWait = Config.getInstance().secondsToWait;
    		int millisToWait = SecondsToWait * 1000;
			Thread.sleep(millisToWait);
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(newLine + "generating dictionary");
		
		for(String tag: tags)
		{
			if (!Config.getInstance().onlyLookInsideElement.equals(""))
			{
				elements.addAll(_driver.findElement(By.className(Config.getInstance().onlyLookInsideElement)).findElements(By.tagName(tag)));
				//printDot('-');
			}
			else
			{
				elements.addAll(_driver.findElements(By.tagName(tag)));
				//printDot('+');
			}
		}
		
		//System.out.println(" elements(" + elements.size() + ") ");
		
		for(WebElement element : elements)
		{
			nonUniqueText.add(element.getText());
			//printDot('~');
		}
		
		//System.out.println(" nonUniqueText(" + nonUniqueText.size() + ") ");
		
		for (String text : nonUniqueText)
		{
			String textWithoutPunctuation = removePunctuation(text);
			//System.out.println(text + " => " + textWithoutPunctuation);
			String[] textToWords = textWithoutPunctuation.split(" ");
			
			for (String word : textToWords)
			{
				if (!word.equals(" "))
				{
					if (!word.equals(""))
					{
						nonUniqueWords.add(word.toUpperCase());
						//printDot('#');
					}
				}
			}
		}
		
		List<String> uniqueWords = new ArrayList<String>(new HashSet<String>(nonUniqueWords));
				
		//System.out.println(" nonUniqueWords(" + uniqueWords.size() + ") ");
		
		
		String[] dictionary = new String[uniqueWords.size()];
		
		for (int i = 0; i < uniqueWords.size(); i++)
		{
			String currentWord = uniqueWords.get(i);
			dictionary[i] = currentWord;
			printDot();
		}
		
		System.out.println(newLine + "Done! the dictionary has " + dictionary.length + " English words" + newLine);
		resetDotIndex();
		return dictionary;
	}

	public static String[] generateUniqueDictionaryFromElement(WebElement _element)
	{
		List<String> nonUniqueWords = new ArrayList<String>();
		String text = _element.getText();
		String textWithoutPunctuation = Util.removePunctuation(text);
		String[] textToWords = textWithoutPunctuation.split(" ");
		
		for (String word : textToWords)
		{
			if (!word.equals(" "))
			{
				if (!word.equals(""))
				{
					nonUniqueWords.add(word.toUpperCase());
				}
			}
		}
		
		List<String> uniqueWords = new ArrayList<String>(new HashSet<String>(nonUniqueWords));
		
		
		String[] dictionary = new String[uniqueWords.size()];
		
		for (int i = 0; i < uniqueWords.size(); i++)
		{
			String currentWord = uniqueWords.get(i);
			dictionary[i] = currentWord;
		}
		return dictionary;
		
	}

	public static ListGatheredWebElement gatherNonEnglishWebElementsFromPage(WebDriver driver)
	{
		int count = 0;
		ListGatheredWebElement output = new ListGatheredWebElement();
		List<WebElement> elements = Util.getElementsFromURL(driver, Config.getInstance().otherURL);
		int nextWebElementID = 0;
		
		for(WebElement element : elements)
		{
			if (!element.getText().equals(""))
			{
				output.add(element, ++nextWebElementID);
			}
			else
			{
				count++;
			}
		}
		
		System.out.println(count + " blank elements were found and ignored");
		
		return output;
	}

	public static void highlightElementsAndScreenShot(WebDriver driver, ListGatheredWebElement gatheredNonEnglishElements)
	{
		for (int i = 0; i < gatheredNonEnglishElements.size(); i++)
		{
			GatheredWebElement currentElement = gatheredNonEnglishElements.get(i);
			
			if (currentElement.isDuplicate)
			{
				System.out.println("found a duplicate element with ID: " + currentElement.getID() + " and text: " + currentElement.getWebElement().getText());
			}
			
			if (currentElement.m_offendingWords.size() > 0)
			{
				
				String colour = currentElement.getWebElement().getCssValue("color");
				try
				{
					highlightElement(driver, currentElement.getWebElement(), Config.getInstance().hiddenConfig.highlightColour, Config.getInstance().highlightBorderThickness);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				scrollAndScreenshotElement(driver, currentElement.getWebElement(), currentElement.getID());
				unhighlightElement(driver, currentElement.getWebElement(), colour);
				currentElement.PrintMatchInfoToFile();
				
			}
		}
	}
	
	public static void printElementsHeaderToFile()
	{
        String filePath = Config.getInstance().pathing.outputDir;
		String outputPath = filePath  + "Report.txt";
		
		String elementHeader = "ELEMENTS:" + newLine + "The following elements should be checked manually. The IDs of these elements are printed onto the screenshots" + newLine;
		
    	try
		{
			FileOutputStream _fs = new FileOutputStream(outputPath, true);
			Util.writeStringToOutputFile(elementHeader, _fs);
			_fs.close();
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void createZipFromOutputFolder()
	{
		
		String zipFile = Config.getInstance().pathing.outputDir + "/" + App.versionString + " " + Util.getRunTimeStamp() +  ".zip";
		String srcDir = Config.getInstance().pathing.outputDir;
		
		try {
			
			// create byte buffer
			byte[] buffer = new byte[1024];

			File dir = new File(srcDir);

			File[] files = dir.listFiles();
			
			FileOutputStream fos = new FileOutputStream(zipFile);

			ZipOutputStream zos = new ZipOutputStream(fos);

			for (int i = 0; i < files.length; i++) 
			{
				
				if (!files[i].getName().contains(".zip"))
				{
					System.out.println("Adding file: " + files[i].getName());
	
					FileInputStream fis = new FileInputStream(files[i]);
	
					// begin writing a new ZIP entry, positions the stream to the start of the entry data
					zos.putNextEntry(new ZipEntry(files[i].getName()));
					
					int length;
	
					while ((length = fis.read(buffer)) > 0) 
					{
						zos.write(buffer, 0, length);
					}


				zos.closeEntry();

				// close the InputStream
				fis.close();
				}
				else
				{
					System.out.println("Skipping file: " + files[i].getName());
				}
			}

			// close the ZipOutputStream
			zos.close();
			
			for (int i = (files.length - 1); i >= 0; i--)
			{
				System.out.println("Deleting File: " + files[i].getName());
				FileUtils.forceDelete(files[i]);
			}
			
		}
		catch (IOException ioe) {
			System.out.println("Error creating zip file" + ioe);
		}
	}

	public static void validateConfigValues()
	{
		System.out.println("Validating Config...\n");
		
		//nerf hightlight border thickness to 3
		int maxHighlightBorder = 3;
		Config.getInstance().highlightBorderThickness = (Math.abs(Config.getInstance().highlightBorderThickness) > maxHighlightBorder)? maxHighlightBorder : Math.abs(Config.getInstance().highlightBorderThickness);
	
		//make sure language is selected and exit if it is not
		try
    	{
    		@SuppressWarnings("unused")
			String testLanguage = Config.getInstance().language.trim().toLowerCase();
    	}
    	catch (NullPointerException npe)
    	{
    		System.out.println("Language must be provided for the tool to run. Please provide a language in config.toml and re-run");
    		System.exit(1);
    	}
    	catch (Exception e)
    	{
    		System.out.println("An error occured with language selection: " + e.getMessage());
    	}
		
		//make sure ignoreFilePath is valid and if it is blank/null then use the default
		String ignoreFilePath;
		try
		{
			ignoreFilePath = Config.getInstance().pathing.ignoreFileDir;
			if (ignoreFilePath.equals(""))
			{
				//I know this is bad but if the file path is blank it's the same action as if it's null, so...
				throw new NullPointerException();
			}
			ignoreFilePath.trim();
		}
		catch (NullPointerException npe)
    	{
    		System.out.println("no path to ignore files was provided in config. defaulting to projectDir/ignorefiles/");
    		Config.getInstance().pathing.ignoreFileDir = Util.getFilePath("ignorefiles", "");
    	}
    	catch (Exception e)
    	{
    		System.out.println("An error occured with ignorefile config: " + e.getMessage());
    	}
		
		@SuppressWarnings("unused")
		List<String> pageSpecificIgnore;
		try
		{
			pageSpecificIgnore = Config.getInstance().pageSpecificIgnoreList;
		}
		catch(NullPointerException npe)
		{
			Config.getInstance().pageSpecificIgnoreList = new ArrayList<String>();
		}

		ValidateConfigValue(Config.getInstance().highlightBorderThickness, "highlightBorderThickness");
		ValidateConfigValue(Config.getInstance().tagsToProcess, "tagsToProcess");
		ValidateConfigValue(Config.getInstance().punctuationToIgnore, "punctuationToIgnore");
		ValidateConfigValue(Config.getInstance().englishURL, "englishURL");
		ValidateConfigValue(Config.getInstance().otherURL, "otherURL");
		
		//ValidateConfigValue(Config.getInstance().pageSpecificIgnoreList, "pageSpecificIgnoreList");
		ValidateConfigValue(Config.getInstance().language, "language");
		ValidateConfigValue(Config.getInstance().clickClassNames, "clickClassNames");
		ValidateConfigValue(Config.getInstance().onlyLookInsideElement, "onlyLookInsideElement");
		ValidateConfigValue(Config.getInstance().pathing.outputDir, "pathing.outputDir");
		ValidateConfigValue(Config.getInstance().pathing.ignoreFileDir, "pathing.ignoreFileDir");
		ValidateConfigValue(Config.getInstance().remote.useRemote, "remote.useRemote");
		ValidateConfigValue(Config.getInstance().remote.remoteAddress, "remote.remoteAddress");
		ValidateConfigValue(Config.getInstance().remote.remoteBrowser, "remote.remoteBrowser");
		ValidateConfigValue(Config.getInstance().remote.remoteCapabilities.Test, "remote.remoteCapabilities.Test");
		ValidateConfigValue(Config.getInstance().hiddenConfig.chromeDriverFilename, "hiddenConfig.chromeDriverFilename");
		ValidateConfigValue(Config.getInstance().hiddenConfig.delimiter, "hiddenConfig.delimiter");
		ValidateConfigValue(Config.getInstance().hiddenConfig.dotsPerLine, "hiddenConfig.dotsPerLine");
		ValidateConfigValue(Config.getInstance().hiddenConfig.highlightColour, "hiddenConfig.highlightColour");
		ValidateConfigValue(Config.getInstance().hiddenConfig.supportedLanguages, "hiddenConfig.supportedLanguages");
		
		
		System.out.println("\nFinished Validating Config.");
	}
	
	@SuppressWarnings("unchecked")
	private static <T> void ValidateConfigValue(T value, String valueName)
	{
		String dataType = value.getClass().getTypeName();
		//System.out.println(dataType);
		String dataTypeFormatter = "%s";
		
		switch(dataType)
		{
		case "java.lang.Boolean":
			dataTypeFormatter = "%b";
			break;
		case "java.lang.String":
			dataTypeFormatter = "%s";
			break;	
			
		case "java.lang.Integer":
			dataTypeFormatter = "%d";
			break;
		default:
			System.out.println("Data Type of Config Option " + valueName + " not supported. defaulting to String");
			dataTypeFormatter = "%s";
			break;
		}
		
		String formatString = valueName + " was set to: " + dataTypeFormatter;
		try
		{
			T valueTest = value;
			if(value instanceof String)
			{
				if (value.equals(""))
				{
					value = (T) "<BLANK_STRING>";
				}
			}
			System.out.println(String.format(formatString, valueTest));
		}
		catch(Exception ex)
		{
			System.out.println("could not verify config option: " + valueName);
		}
	}
}
