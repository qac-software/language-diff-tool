package com.qaconsultants.languagedifftool;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

public class GatheredWebElement implements Cloneable
{
	private int m_id;
	private WebElement m_element;
	
	public WebElement getWebElement()
	{
		return m_element;
	}

	public String[] m_words;
	public List<String> m_offendingWords;
	public boolean isDuplicate;
	
	
	private GatheredWebElement()
	{
	}
	
	public GatheredWebElement clone() throws CloneNotSupportedException
	{
		GatheredWebElement clone = new GatheredWebElement();
		clone.m_id = new Integer(this.m_id);
		
		//note: no clone method exists for Selenium WebElement class
		//this could result in problems if you change a webelement on Object A after cloning it Object B (the clone) would also change
		clone.m_element = this.m_element;
		
		String[] clonedWords = new String[m_words.length];
		for (int i = 0; i < m_words.length; i++)
		{
			clonedWords[i] = new String(m_words[i]);
		}
		clone.m_words = clonedWords;
		
		List<String> cloneOffendingWords = new ArrayList<String>();
		for (String offendingWord : this.m_offendingWords)
		{
			cloneOffendingWords.add(new String(offendingWord));
		}
		clone.m_offendingWords = cloneOffendingWords;
		
		clone.isDuplicate = new Boolean(this.isDuplicate);
		
		return clone;
	}
	

	public GatheredWebElement(int _id, WebElement _element)
	{
		this();
		m_element = _element;
		m_words = Util.generateUniqueDictionaryFromElement(_element);
		m_offendingWords = new ArrayList<String>();
		m_id = _id;
		isDuplicate = false;
	}
	
	public void Process(String[] englishDictionary, List<String> ignoreList)
	{
		System.out.println("Processing Gathered WebElement " + m_id);
		System.out.println(m_element.getText());
		for (String nonEnglishWord : m_words)
		{
			for (String englishWord : englishDictionary)
			{
				if (englishWord.toUpperCase().equals(nonEnglishWord.toUpperCase()))
				{
					boolean foundInIgnoreList = false;
					
					for (String ignoreWord : ignoreList)
					{
						if (nonEnglishWord.toUpperCase().equals(ignoreWord.toUpperCase()))
						{
							foundInIgnoreList = true;
							System.out.println("ignored: " + nonEnglishWord);
							break;
						}
					}
					
					if(!foundInIgnoreList)
					{
						m_offendingWords.add(englishWord);
						System.out.println(nonEnglishWord + " = " + englishWord);
					}
					
				}
			}
		}
		System.out.println("Done Processing WebElement " + m_id + Util.newLine);
	}
	
	public int getID()
	{
		return m_id;
	}
	
	public void PrintMatchInfoToFile()
	{
		
        String filePath = Config.getInstance().pathing.outputDir;
		String outputPath = filePath  + "Report.txt";
		
		String matchInfo = "element # " + m_id + ": " + m_element.getText() + " contains the following English words:";
		
		for(String word : m_offendingWords)
		{
			matchInfo += " " + word;
		}
		
    	try
    	{
    	FileOutputStream _fs = new FileOutputStream(outputPath, true);
    	Util.writeStringToOutputFile(matchInfo, _fs);
    	}
    	catch (Exception _ex)
    	{
    		System.out.println(_ex.getMessage());
    	}
	}
}
