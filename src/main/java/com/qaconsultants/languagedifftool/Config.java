package com.qaconsultants.languagedifftool;

import java.util.List;

/***
 * POJO for TOML Config parser
 * @author Alex
 *
 */
class Config 
{
	public class Pathing 
	{
		String outputDir;
		String ignoreFileDir;
	}
	
	public class HiddenConfig
	{
		String chromeDriverFilename;
		String delimiter;
		int dotsPerLine;
		String highlightColour;
		List<String> supportedLanguages;
		
	}
	
	public class Remote
	{
		boolean useRemote;
		String remoteAddress;
		String remoteBrowser;
		
		RemoteCapabilities remoteCapabilities;
	}
	
	public class RemoteCapabilities
	{
		String Test;
	}
	
	/*
	 * 
	 # Remote execution information
	[remote]
	useRemote = true
	remoteAddress = "192.168.2.1"
	remoteBrowser = "CHROME"
	
	[remote.remoteCapabilities]
	Test = "Test"
	 * 
	 */
	
	//String chromeDriverFilename;
	
	//String highlightColour;
	int highlightBorderThickness;
	
	//String delimiter;
	
	String englishURL;
	String otherURL;
	
	int secondsToWait;
	
	List<String> tagsToProcess;
	List<String> clickClassNames;
	List<String> punctuationToIgnore;
	
	//int dotsPerLine;
	
	String onlyLookInsideElement;
	
	String language;
	
	List<String> pageSpecificIgnoreList;
	
	List<String> frenchIgnoreList;
	List<String> germanIgnoreList;
	List<String> arabicIgnoreList;
	List<String> japaneseIgnoreList;
	List<String> chineseIgnoreList;
	List<String> spanishIgnoreList;
	List<String> russianIgnoreList;
	List<String> italianIgnoreList;
	List<String> koreanIgnoreList;
	List<String> portugueseIgnoreList;
	List<String> brazilianPortugueseIgnoreList;
	
	
	Pathing pathing;
	HiddenConfig hiddenConfig;
	Remote remote;
	
	
	public static Config instance = null;
	
	/***
	 * Constructor - sets the static instance for the "faux-singleton"
	 */
	public Config()
	{
		System.out.println("Constructing the Config class...");
		instance = this;
	}
	
	/**
	 * gets the static instance of the config class
	 * @return the config class instance
	 */
	public static Config getInstance()
	{
		assert instance!=null;
		return instance;
	}
}
