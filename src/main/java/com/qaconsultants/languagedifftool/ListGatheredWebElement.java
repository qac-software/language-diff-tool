package com.qaconsultants.languagedifftool;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebElement;

public class ListGatheredWebElement
{
	private ArrayList<GatheredWebElement> m_elements;
	private int m_size;
	
	public ListGatheredWebElement()
	{
		m_elements = new ArrayList<GatheredWebElement>();
	}
	
	public List<Integer> IDSet()
	{
		List<Integer> IDs = new ArrayList<Integer>();
		
		for (GatheredWebElement gwe : m_elements)
		{
			IDs.add(gwe.getID());
		}
		
		return IDs;
	}
	
	public boolean add(GatheredWebElement gweInstance)
	{
		boolean success = false;
		
		try
		{
		m_elements.add(gweInstance);
		m_size++;
		success = true;
		}
		catch (Exception exception)
		{
			System.out.println(exception.getMessage());
		}

		return success;
	}
	
	public boolean add(WebElement _webElement, int _id)
	{
		boolean success = false;
		
		try
		{
		m_elements.add(new GatheredWebElement(_id, _webElement));
		m_size++;
		success = true;
		}
		catch (Exception exception)
		{
			System.out.println(exception.getMessage());
		}

		return success;
	}
	
	public GatheredWebElement get(int _index)
	{
		return m_elements.get(_index);
	}
	
	public GatheredWebElement get(GatheredWebElement _gwe)
	{
		boolean found = false;
		int foundIndex = 0;
		
		for (GatheredWebElement gatheredWebElement: m_elements)
		{
			if (_gwe.getID() == gatheredWebElement.getID())
			{
				found = true;
				break;
			}
			foundIndex++;
		}
		
		if (found)
		{
			return m_elements.get(foundIndex);
		}
		else
		{
			return null;
		}
		
	}
	
	public List<WebElement> getWebElementsByID(int _ID)
	{
		List<WebElement> output = new ArrayList<WebElement>();
		
		for (GatheredWebElement gatheredWebElement: m_elements)
		{
			if (_ID == gatheredWebElement.getID())
			{
				output.add(gatheredWebElement.getWebElement());
			}
		}
		
		return output;
	}
	
	public WebElement getWebElementByID(int _ID)
	{
		WebElement output = null;
		
		for (GatheredWebElement gatheredWebElement: m_elements)
		{
			if (_ID == gatheredWebElement.getID())
			{
				output = gatheredWebElement.getWebElement();
				break;
			}
		}
		
		return output;
	}
	
	public boolean remove(GatheredWebElement _gwe)
	{
		boolean success = false;
		
		try
		{
			m_elements.remove(_gwe);
			m_size--;
		}
		catch (Exception exception)
		{
			System.out.println(exception.getMessage());
		}
		
		return success;
	}
	
	public boolean remove(int _index)
	{
		boolean success = false;
		
		try
		{
			m_elements.remove(_index);
			m_size--;
		}
		catch (Exception exception)
		{
			System.out.println(exception.getMessage());
		}
		
		return success;
	}
	
	public int size()
	{
		return m_size;
	}
	
	public void ProcessElements(String[] englishDictionary, List<String> ignoreList)
	{
		for (GatheredWebElement gwe : m_elements)
		{
			gwe.Process(englishDictionary, ignoreList);
		}
	}
	
//	public void flagDuplicates()
//	{
//		System.out.println("processing to remove duplicates");
//		List<Integer> ids = IDSet();
//		List<Integer> removeTheseIDS = new ArrayList<Integer>();
//		List<Integer> flaggedIDs = new ArrayList<Integer>();
//		
//		for (Integer id: ids)
//		{
//			if(getWebElementByID(id).getText().equals(""))
//			{
//				removeTheseIDS.add(id);
//			}
//		}
//		
//		ids.removeAll(removeTheseIDS);
//		
//		for (Integer id : ids)
//		{
//			Util.printDot();
//			for (Integer otherID : ids)
//			{
//				//System.out.println("" + id + " vs. " + otherID);
//				if (id != otherID)
//				{
//					//System.out.println("checking for duplicate");
//					if (flaggedIDs.size() > 0)
//					{
//						int idToFlag = -1;
//						for (int flaggedID : flaggedIDs)
//						{
//							if (id != flaggedID)
//							{
//								//System.out.println(id + " isn't flagged");
//								if (otherID != flaggedID)
//								{
//									//System.out.println(otherID + " isn't flagged");
//									
//									//System.out.println(id + " with text: " + getWebElementByID(id).getText());
//									//System.out.println(otherID + " with text: " + getWebElementByID(otherID).getText());
//									
//									if (getWebElementByID(id).getText().equals(getWebElementByID(otherID).getText()))
//									{
//										System.out.println(id + " and " + otherID + " are equal");
//										get(id).isDuplicate = true;
//										//flaggedIDs.add(id);
//										idToFlag = id;
//									}
//								}
//							}
//						}
//						if (idToFlag != -1)
//						{
//							flaggedIDs.add(idToFlag);
//						}
//					}
//					else
//					{
//						if (getWebElementByID(id).getText().equals(getWebElementByID(otherID).getText()))
//						{
//							System.out.println(id + " and " + otherID + " are equal");
//							get(id).isDuplicate = true;
//							flaggedIDs.add(id);
//						}
//					}
//				}
//			}
//		}
//		System.out.println("Done!");
//	}
	
	public void flagDuplicates()
	{
		System.out.println("processing to remove duplicates");
		ArrayList<GatheredWebElement> uniqueElements = new ArrayList<GatheredWebElement>();
		System.out.println("Processing duplicates...");
		for (GatheredWebElement gwe : m_elements)
		{
			Util.printDot();
			if (uniqueElements.size() == 0)
			{
				uniqueElements.add(gwe);
			}
			else
			{
				boolean found = false;
				for (GatheredWebElement ugwe : uniqueElements)
				{
					if (ugwe.getWebElement().getText().equals(gwe.getWebElement().getText()))
					{
						found = true;
						break;
					}
				}
				
				if (!found)
				{
					uniqueElements.add(gwe);
				}
			}
		}
		System.out.println("Done!");
		m_elements = uniqueElements;
		m_size = uniqueElements.size();
	}
	
	
}
